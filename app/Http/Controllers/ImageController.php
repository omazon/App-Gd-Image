<?php

namespace App\Http\Controllers;

use GDText\Box;
use GDText\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('image.index');
//        $im = imagecreatefromstring(file_get_contents(Input::file('image')));
//        $box = new Box($im);
//        $box->setFontFace('uploads/bold.ttf'); // http://www.dafont.com/prisma.font
//        $box->setFontSize(30);
//        $box->setFontColor(new Color(148, 212, 1));
//        $box->setTextShadow(new Color(0, 0, 0, 50), 0, -2);
//        $box->setBox(90, 20, 460, 460);
//        $box->setTextAlign('center', 'center');
//        $box->draw("lorem asdafjdsfa lorem asdafjdsfa lorem asdafjdsfa lorem asdafjdsfa lorem asdafjdsfa ");
//        header("Content-type: image/png");
//        imagepng($im);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $im = imagecreatefromstring(file_get_contents(Input::file('image')));
        $box = new Box($im);
        $box->setFontFace('uploads/bold.ttf'); // http://www.dafont.com/prisma.font
        $box->setFontSize(30);
        $box->setFontColor(new Color(148, 212, 1));
        $box->setTextShadow(new Color(0, 0, 0, 50), 0, -2);
        $box->setBox(90, 20, 460, 460);
        $box->setTextAlign('center', 'center');
        $box->draw("lorem asdafjdsfa lorem asdafjdsfa lorem asdafjdsfa lorem asdafjdsfa lorem asdafjdsfa ");
        header("Content-type: image/png");
        imagepng($im,'uploads/'.str_random(10).'.jpg');
        return;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
