<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <h1>Prueba de manipulación de imagenes</h1>
            <p>usando la libreria intervention image</p>
        </div>
        {!! Form::open(['route'=>'image.store','method'=>'post','files'=>true,'class'=>'form']) !!}
            {!! Form::file('image') !!}
            {!! Form::submit('Subir',['class'=>'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>